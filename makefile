OFILE=clp
FILES=main.c
LIBS=-lm
COMPILE=gcc -std=c11 -pedantic -Wall -fno-strict-aliasing \
        $(FILES) -o $(OFILE) $(LIBS) 2>&1

build:
	$(COMPILE)

e:
	$(COMPILE) -fdiagnostics-color=always | less -r

small:
	$(COMPILE) -Os

clean:
	rm main.o clp main

debug:
	$(COMPILE) -g

valgrind:
	$(COMPILE) -g && valgrind --track-origins=yes --leak-check=full ./clp  2>&1

callgrind:
	$(COMPILE) -g && valgrind --tool=callgrind ./clp  2>&1

gdb:
	$(COMPILE) -g && gdb clp
