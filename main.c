#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <assert.h>

#define VERSION 0.4
/* (... 0 n) => (0 1 2 ... n)
 * (print (* (... 0 12) n) 'newline)
 * (set n 5) (repeat n [(0 1 2 3 4 5)] (print n))
 *
 * TODO: Things to invoke bugs (So I won't forget them)
 *
 */

typedef int boolean;

enum {
  NREF = -1,
  NIL,
  NUMBER,
  CONS,
  STRING,
  SYMBOL,
  NATIVE,
  ENVIRO
};
typedef int64_t ref;
typedef size_t sym;
typedef ref env;
typedef uint64_t number;

typedef ref (*nativef)(ref, env);
typedef struct data_t {
  int type, bound;
  ref result;
  union {
    struct { ref car, cdr; };
    nativef f;
    struct { number d, n; }; /* format: n.d */
    struct { sym id; ref v; env next; };
    sym sym;
    char *s;
  };
} data_t;

/* Buffers to keep track of memory */
#define TOKENSIZE   512   /* Tested with lower bound of 3 */
#define SYMBOLSIZE  512
#define DATASIZE    512

#define BUFSIZE       4   /* Default memory size of symbols */

struct {
  data_t *pool;
  boolean *isfree;
  size_t size, start;
} data;

/* So apparently this idea already exists, and is called an 'oblist' */
struct {
  struct symbol_o {
    char *s;
    ref d;
  } *pool;
  size_t size, i;
} symlist;
typedef struct symbol_o symbol_o;

struct {
  char **stack;
  size_t size, i;
} tokens;

typedef struct func_t {
  ref f;
  symbol_o *s;
} func_t;

struct {
  unsigned int print_depth;
} state;

sym SNIL, STRUE, SERR, SREPLABORT, SLAMBDA, SPROCEDURE, SGLOBALENV;
sym SPRINTDEPTH;
env GLOBALENV;

/* function definitions (In the order that they are presented) */
static void    xfail(char *, int);
static void    hwarn(char *, int);

static void    tokens_init(size_t);
static void    tokens_add(char *);
static void    tokens_grow(void);
static void    tokens_clean(void);
static void    tokens_deinit(void);

static void    sym_init(size_t);
static void    sym_grow(void);
static sym     sym_new(char*);
static sym     sym_add(char*);
static void    sym_deinit(void);

static env     env_inherit(env, env);
static env     env_new(void);
static env     env_copy(env);
static env     env_end(env);
static ref     env_set(env, sym, ref);
static env     env_find(env, sym);

static void    init_symbols(void);
       int     main(int, char **);
static char*   strdup(const char *);
static int     lstrcmp(char *, char *);

static void    data_init(size_t);
static void    data_deinit(void);
static ref     data_new(void);
static void    data_del(ref);
static void    data_grow(void);
static void    free_all(void);
/* TODO: Add safe getter for data, and replace all instances of data.pool[n] */

static ref     cons(ref, ref);
static ref     native(nativef);
static ref     num(number, number);
static ref     string(char *);
static ref     symbol(char *);
static sym     res_symbol(char *, env, ref);
static sym     gensym(void);

static int     type(ref );
static void    print_types(ref);
static void    print(ref);
static void    pprint(ref);
static ref     eval(ref, env);

ref            first(ref);
ref            rest(ref);

ref            n_nth(ref, env);
ref            n_car(ref, env);
ref            n_cdr(ref, env);
ref            n_quote(ref, env);
ref            n_cons(ref, env);
ref            n_set(ref, env);
ref            n_eset(ref, env);
ref            n_setcar(ref, env);
ref            n_setcdr(ref, env);
ref            n_pragma(ref, env);
ref            n_type(ref, env);
ref            n_apply(ref, env);
ref            n_env(ref v, env e);
ref            n_plus(ref, env);
ref            n_gensym(ref, env);

static char*   read_varsize(FILE *f, int(*isendc)(int), boolean unget);
static ref     read_token(FILE *);
static ref     read(FILE *f, ref);

static int     repl(int debug);

#define fail(s)  xfail(s, __LINE__);
#define warn(s)  hwarn(s, __LINE__);
#define abort(s) warn(s)

static void xfail(char *s, int line)
/* Handler for fail(). Prints an error message and line num. USE FAIL INSTEAD */
{
  printf("\nFail: %s at line %d\n", s, line);
  exit(1);
}

static void hwarn(char *s, int line)
/* Handler for warn(). Prints warning and line number. USE WARN INSTEAD */
{
  printf("\nWarn: %s at line %d\n", s, line);
}

static void tokens_init(size_t size)
{
  tokens.stack = calloc(size, sizeof(char*));
  if (!tokens.stack) fail("Unable to allocate the token list");
  tokens.size  = size;
  tokens.i     = 0;
}

static void tokens_add(char *s)
{
  if (tokens.i == tokens.size) tokens_grow();
  tokens.stack[tokens.i++] = s;
}

static void tokens_grow(void)
{
  tokens.size += tokens.size;
  tokens.stack = realloc(tokens.stack, tokens.size*sizeof(char*));
  if (!tokens.stack) fail("Unable to grow the token list");
}

static void tokens_clean(void)
{
  size_t i;
  for (i = 0; i < tokens.i; i++)
    free(tokens.stack[i]);

  tokens.i = 0;
}

static void tokens_deinit(void)
{
  tokens_clean();
  free(tokens.stack);
}

static void sym_init(size_t size)
/* Initalize the symbol list */
{
  symlist.pool = calloc(size, sizeof(symbol_o));
  if (!symlist.pool) fail("Failed to allocate the symbol list");
  symlist.size = size;
  symlist.i    = 0;
}

static void sym_grow(void)
/* Increases the amount of space for symbols */
{
  size_t s = symlist.size*symlist.size;
  symlist.pool = realloc(symlist.pool, s*sizeof(symbol_o));
  if (!symlist.pool) fail("Failed to grow the symbol list");

  memset(&symlist.pool[symlist.size], 0, sizeof(symbol_o)*(s-symlist.size));
  symlist.size = s;
}

static sym sym_new(char *s)
/* Creates a new symbol, grows the list of symbols if it's not big enough */
{
  sym i = symlist.i++;
  symlist.pool[i].s = strdup(s);

  ref d = data_new();
  data.pool[d].type = SYMBOL;
  data.pool[d].sym = i;

  symlist.pool[i].d = d;

  if (symlist.i == symlist.size)
    sym_grow();

  return i;
}

static sym sym_add(char *s)
/* Returns an existing symbol, or creates a new one and returns it */
{
  size_t i;
  for (i = 0; i < symlist.i; i++) {
    if (lstrcmp(s, symlist.pool[i].s) == 0)
      return i;
  }
  return sym_new(s);
}

static void sym_deinit(void)
/* Initalize the symbol list */
{
  size_t i;
  for (i = 0; i < symlist.i; i++) {
    free(symlist.pool[i].s);
  }
  free(symlist.pool);
}

static env env_new(void)
/* Creates a new environment object */
{
  env e = data_new();
  data.pool[e].type = ENVIRO;
  data.pool[e].id = data.pool[e].v = data.pool[e].next = NREF;
  return e;
}
static env env_copy(env e)
{
  env te = data_new();
  data.pool[te].type = ENVIRO;
  data.pool[te].id = data.pool[e].id;
  data.pool[te].v = data.pool[e].v;
  data.pool[te].next = NREF;
  return te;
}
static env env_end(env e)
{
  while (data.pool[e].next != NREF)
    e = data.pool[e].next;
  return e;
}
static env env_inherit(env root, env parent)
/* Inherits bindings from a parent environment, disregarding if the binding
 * exists already */
{
  if (root == NREF || parent == NREF)
    fail("Attempted to inherit from nil environment!");

  env e = env_end(root);
  do {
    data.pool[e].next = env_copy(parent);

    e = data.pool[e].next;
    parent = data.pool[parent].next;
  } while (data.pool[parent].next != NREF);

  return root;
}
static env env_set(env e, sym s, ref v)
/* Updates or creates a new binding in env */
{
  env te = env_find(e, s);
  if (te == NREF) {
    te = env_end(e);
    te = data.pool[te].next = env_new();
  }
  data.pool[te].id = s;
  data.pool[te].v  = v;

  return e;
}
static env env_find(env e, sym s)
{
  if (e == NREF) return NREF;
  while (data.pool[e].next != NREF && data.pool[e].id != s)
    e = data.pool[e].next;

  return (data.pool[e].id == s) ? e : NREF;
}

static void init_symbols(void)
/* Reserves and binds some symbols on initalization */
{
  GLOBALENV   = env_new();
  SGLOBALENV  = res_symbol("$env-glob",    GLOBALENV, NREF);
  SNIL        = res_symbol("nil",          GLOBALENV, NREF);
  STRUE       = res_symbol("t",            GLOBALENV, NREF);
  SERR        = res_symbol("error",        GLOBALENV, NREF);
  SREPLABORT  = res_symbol("$quit",        GLOBALENV, NREF);
  SPROCEDURE  = res_symbol("$procedure",   GLOBALENV, NREF);
  SPRINTDEPTH = res_symbol("$print-depth", GLOBALENV, NREF);
  /* TODO: Add code to set global objects as mutable? */
  res_symbol("car",     GLOBALENV, native(n_car));
  res_symbol("cdr",     GLOBALENV, native(n_cdr));
  res_symbol("cons",    GLOBALENV, native(n_cons));
  res_symbol("quote",   GLOBALENV, native(n_quote));
  res_symbol("set",     GLOBALENV, native(n_set));
  res_symbol("eset",    GLOBALENV, native(n_eset));
  res_symbol("set-car", GLOBALENV, native(n_setcar));
  res_symbol("set-cdr", GLOBALENV, native(n_setcdr));
  res_symbol("type",    GLOBALENV, native(n_type));
  res_symbol("apply",   GLOBALENV, native(n_apply));
  res_symbol("+",       GLOBALENV, native(n_plus));
  res_symbol("gensym",  GLOBALENV, native(n_gensym));
  res_symbol("pragma",  GLOBALENV, native(n_pragma));
  res_symbol("nth",     GLOBALENV, native(n_nth));
}

int main(int argc, char **argv)
{
  state.print_depth = 9;

  if (argc > 1)
  {
    int i;
    for (i = 1; i < argc; i++) {
      if (lstrcmp(argv[i], "-help") == 0) {
        printf("clp -- C LisP. A simple lispy language.\n"
               "Version %g, Finn O'leary (@gallefray).\n"
               "Options:\n"
               "  -help\n", VERSION);
        exit(1);
      }
    }
  }

  tokens_init(TOKENSIZE);
  data_init(DATASIZE);
  sym_init(SYMBOLSIZE);
  init_symbols();

  /* REPL? Sure! */
  repl(0);
  free_all();
  tokens_deinit();

  return 0;
}

static char *strdup(const char *s)
{
  char *t = calloc(strlen(s)+1, 1);
  if (t) memcpy(t, s, strlen(s));
  if (!t) fail("Could not duplicate string!");
  return t;
}

static int lstrcmp(char *s, char *t)
/* Compares two strings in a case-insensitive manner */
{
  for (;; s++, t++) {
    if ((tolower(*s) - tolower(*t)) == 0) {
      if (*s == '\0') return 0;
    }
    else break;
  }
  return tolower(*s) - tolower(*t);
}

static void data_init(size_t size)
/* Sets up the data object */
{
  data.pool = calloc(size, sizeof(data_t));
  if (!data.pool) fail("Could not allocate data pool");
  data.isfree = calloc(size, sizeof(boolean));
  if (!data.isfree) fail("Could not allocate data pool");
  data.size = size;
  data.start = 0;
}

static void data_deinit(void)
/* Frees the data object */
{
  ref i;
  for (i = 0; i < data.size; i++) {
    if (data.isfree[i])
      data_del(i);
  }
  free(data.pool);
  free(data.isfree);
}

static ref data_new(void)
/* Removes the object from the free list and finds the next free item */
{
  ref r = data.start;
  data.isfree[data.start] = 1;

  /* init the data */
  data.pool[r].car = data.pool[r].cdr = data.pool[r].result = NREF;
  data.pool[r].type = NIL;

  /* Find the next free item */
  for (; data.isfree[data.start]; data.start++) {
    if (data.start+1 == data.size)
      data_grow();
  }
  return r;
}

static void data_del(ref r)
/* Adds the object to the free list (Does not zero data) */
{
  data.isfree[r] = 0;
  if (data.pool[r].type == STRING)
    free(data.pool[r].s);
  data.start = r;
}

static void data_grow(void)
{
  size_t s = data.size;
  data.size *= data.size;

  data.pool   = realloc(data.pool, data.size*sizeof(data_t));
  if (!data.pool) fail("Could not resize data pool");
  data.isfree = realloc(data.isfree, data.size*sizeof(boolean));
  if (!data.isfree) fail("Could not resize data pool");

  size_t i;
  for (i = s; i < data.size; i++)
    data.isfree[i] = 0;
}

static void free_all(void)
/* Frees all the data structures in existence */
{
  sym_deinit();
  data_deinit();
}

static ref cons(ref car, ref cdr)
/* Return a new cons cell */
{
  ref r = data_new();
  data.pool[r].car  = car;
  data.pool[r].cdr  = cdr;
  data.pool[r].type = CONS;

  return r;
}

static ref native(nativef f)
/* Return a new representation of a native function f */
{
  ref r = data_new();
  data.pool[r].f = f;
  data.pool[r].type = NATIVE;

  return r;
}

static ref num(number n, number d)
/* Return a new number */
{
  ref r = data_new();
  data.pool[r].n = n;
  data.pool[r].d = d;
  data.pool[r].type = NUMBER;

  return r;
}

static ref string(char *s)
/* Return a ref string copy */
{
  ref r = data_new();
  data.pool[r].s = strdup(s);
  if (!data.pool[r].s) fail("Could not create new string!");
  data.pool[r].type = STRING;

  return r;
}

static ref symbol(char *s)
/* Returns a reference to a symbol, creates a new symbol if necessary */
{
  ref r = data_new();
  data.pool[r].sym = sym_add(s);
  data.pool[r].type = SYMBOL;

  return r;

  /* TODO: This optimized code breaks read and makes everything return nil,
   * probably because I'm using symbol() somewhere I shouldn't be?
   *
   * sym ts = sym_add(s);
   * return symlist.pool[ts].d;
   * */
}

static sym res_symbol(char *s, env e, ref r)
/* Reserve a symbol, assign data to it and return */
{
  sym ts = sym_add(s);
  if (e != NREF && r != NREF)
    env_set(e, ts, r);

  return ts;
}

/* TODO: Refactor function to make less expensive */
static sym gensym(void)
{
  static int n = 1;
  static char buf[5+21+1];
  char *pt = buf+5;

  strcpy(buf, "$gen:");

  int i;
  for (i = n; i > 0; i = i / 10)
    *pt++ = '0' + (n%10);

  *pt = '\0';
  n++;

  return sym_add(buf);
}

static int type(ref v)
/* Grab the type from a given structure, safely */
{
  if (v == NREF) return NIL;
  return data.pool[v].type;
}

static void print_type(int t)
/* Print a character representation of a type */
{
  switch (t) {
    case NIL:
      printf("nil");
      break;
    case NUMBER:
      printf("number");
      break;
    case CONS:
      printf("cons");
      break;
    case STRING:
      printf("string");
      break;
    case SYMBOL:
      printf("symbol");
      break;
    case NATIVE:
      printf("native");
      break;
    case ENVIRO:
      printf("environment");
      break;
  }
}

static void print_types_rec(ref v)
/* Recursively print the types of a structure, used for debugging */
{
  static int print_depth = 0;
  print_depth++;
  sym t = type(v);
  switch (t) {
    case CONS:
      if (print_depth > state.print_depth) {
        printf("...");
        break;
      }
      printf("(");
      print_types_rec(data.pool[v].car);
      printf(" ");
      print_types_rec(data.pool[v].cdr);
      printf(")");
      break;
    default:
      print_type(t);
      break;
  }
  print_depth--;
}

static void print_types(ref v)
/* Recursively print the types of a structure and append a newline */
{
  print_types_rec(v);
  printf("\n");
}

static void print_rec(ref v, int pretty)
/* Recursively print the values of a structure */
{
  static int print_depth = 0;
  sym t = type(v);
  print_depth++;

  switch (t) {
    case NUMBER:
      printf("%"PRIu64".%"PRIu64, data.pool[v].n, data.pool[v].d);
      break;
    case SYMBOL:
      printf("%s", symlist.pool[data.pool[v].sym].s);
      break;
    case CONS:
      if (print_depth > state.print_depth) {
        printf("...");
        break;
      }
      printf("(");
      print_rec(data.pool[v].car, pretty);
      if (!pretty || data.pool[v].cdr != NREF) {
        printf(" ");
        print_rec(data.pool[v].cdr, pretty);
      }
      printf(")");
      break;
    case STRING:
      printf("\'%s\'", data.pool[v].s);
      break;
    case NATIVE:
      printf("f:%lu", (uintptr_t)data.pool[v].f);
      break;
    case NIL:
      printf("nil");
      break;
    case ENVIRO:
      printf("{");
      while (v != NREF) {
        if (data.pool[v].id != NREF)
          printf("%s", symlist.pool[data.pool[v].id].s);
        printf(": ");
        print_rec(data.pool[v].v, pretty);
        if (data.pool[v].next != NREF) printf(", ");

        v = data.pool[v].next;
      }
      printf("}");
      break;
    default:
      printf("type: %lu", t);
      warn("Unrecognised type");
      break;
  }

  print_depth--;
}

static void print(ref v)
/* Recursively print the values of a structure and append a newline */
{
  print_rec(v, 0);
  printf("\n");
}

static void pprint(ref v)
/* Print a 'pretty' version of v */
{
  print_rec(v, 1);
  printf("\n");
}

static ref store(ref in, ref result)
/* Stores result in (in->result) and returns result */
{
  if (in == NREF) return result;

  data.pool[in].result = result;
  return result;
}

#define sreturn(foo) return store(v, foo);

static ref eval(ref v, env e)
/* Lazily evaluate the data if not already evaluated, and return the result */
{
  if (e == NREF) e = GLOBALENV;
  if (v != NREF && data.pool[v].result != NREF) return data.pool[v].result;

  int t = type(v);
  ref d;
  switch (t) {
    case NIL: case NREF:
      sreturn(symlist.pool[SNIL].d);
    case SYMBOL:
      e = env_find(e, data.pool[v].sym);
      if (e == NREF) e = env_find(e, SNIL);

      return data.pool[e].v; /* TODO: Memoize? */
    case CONS:
      if (!first(v)) sreturn(symlist.pool[SERR].d);
      sreturn(n_apply(v, e));
    case NUMBER:   /* FALLTHROUGH */
    case STRING:
    case NATIVE:
      sreturn(v);
    default:
      warn("Should not hit this case");
      break;
  }
  sreturn(symlist.pool[SERR].d);
}

ref n_nth(ref v, env e)
{
  ref num  = eval(first(v), e);
  ref cons = eval(first(rest(v)), e);
  if (data.pool[num].type != NUMBER || data.pool[cons].type != CONS)
    return symlist.pool[SERR].d;


  number n = data.pool[num].n;
  ref pt;
  for (pt = cons; n > 0 && pt > 0; n--)
    pt = rest(pt);

  if (pt == NREF) return symlist.pool[SNIL].d;
  else return first(pt);
}

ref first(ref v)
/* For argument handling */
{
  if (type(v) == NIL || type(data.pool[v].car) == NIL) return NREF;
  return data.pool[v].car;
}

ref rest(ref v)
/* For argument handling */
{
  if (type(v) == NIL || type(data.pool[v].car) == NIL) return NREF;
  return data.pool[v].cdr;
}

ref n_car(ref v, env e)
/* clp: return car */
{
  v = eval(first(v), e);
  if (type(v) != CONS) return symlist.pool[SERR].d;
  return data.pool[v].car;
}
ref n_cdr(ref v, env e)
/* clp: return cdr */
{
  v = eval(first(v), e);

  if (type(v) != CONS) return symlist.pool[SERR].d;
  return data.pool[v].cdr;
}
ref n_cons(ref v, env e)
/* clp: return cons */
{
  return v;
}
ref n_quote(ref v, env e)
/* clp: return v */
{
  if (type(v) != CONS) return v;
  return data.pool[v].car;
}
ref n_set(ref v, env e)
/* Sets a symbol value to an evaluated argument */
{
  ref f = first(v);
  ref r = rest(v);
  if (type(f) != SYMBOL || r == NREF || data.pool[f].sym == SNIL)
    return symlist.pool[SERR].d;
  sym n = data.pool[f].sym;

  return env_set(e, n, eval(first(r), e));
}
ref n_eset(ref v, env e)
/* Sets the result of an evaluation to an evaluated argument */
{
  ref f = eval(first(v), e);
  ref r = rest(v);
  if (type(f) != SYMBOL || data.pool[f].sym == SNIL)
    return symlist.pool[SERR].d;
  sym n = data.pool[f].sym;

  return env_set(e, n, eval(first(r), e));
}
ref n_setcar(ref v, env e)
{
  ref cons = eval(first(v), e);
  ref to = eval(first(rest(v)), e);
  if (type(cons) != CONS) return symlist.pool[SERR].d;
  return data.pool[cons].car = eval(to, e);
}
ref n_setcdr(ref v, env e)
{
  ref cons = eval(first(v), e);
  ref to = first(rest(v));
  if (type(cons) != CONS) return symlist.pool[SERR].d;
  return data.pool[cons].cdr = eval(to, e);
}
ref n_pragma(ref v, env e)
/* Allows the user to set directives that let them influence the interpreter */
{
  if (type(v) != CONS || type(data.pool[v].car) != SYMBOL)
    return symlist.pool[SERR].d;

  sym s = data.pool[data.pool[v].car].sym;
  if (s == SPRINTDEPTH) {
    ref r = first(rest(v));
    number n = data.pool[r].n;
    state.print_depth = n;
  }
  else {
    return symlist.pool[SNIL].d;
  }
  return symlist.pool[STRUE].d;
}
ref n_type(ref v, env e)
/* Returns the type of a value */
{
  /* TODO: Return a symbol here */
  int n = type(first(v));
  switch (n) {
    case NIL:
      return symbol("nil");
    case NUMBER:
      return symbol("number");
    case CONS:
      return symbol("cons");
    case STRING:
      return symbol("string");
    case SYMBOL:
      return symbol("symbol");
    case NATIVE:
      return symbol("native");
    default:
      break;
  }
  return symbol("error");
}
ref n_apply(ref v, env e)
/* Applies the first argument to the values contained in the second (a list) */
{
  if (type(v) != CONS) return symlist.pool[SERR].d;

  ref f = first(v);
  f = eval(f, e);

  if (type(f) == NATIVE) {
    f = eval(f, e);
    ref arg = rest(v);
    if (!data.pool[f].f) return symlist.pool[SERR].d;
    if (arg == NREF) arg = symlist.pool[SNIL].d;
    return (*(data.pool[f].f))(arg, e);
  }
  else if (type(f) == CONS) {
    if (data.pool[first(f)].sym == SPROCEDURE) {
      ref var  = first(rest(f));
      ref body = first(rest(rest(f)));
      ref envsym  = first(rest(rest(rest(f))));
      pprint(body);
      return eval(body, symlist.pool[envsym].d);
    }
  }
  return NREF;
}
ref n_env(ref v, env e)
{
  return env_new();
}
ref n_plus(ref v, env e)
{
  number n0 = data.pool[eval(first(v), e)].n;
  number d0 = data.pool[eval(first(v), e)].d;
  number n1 = data.pool[eval(first(rest(v)), e)].n;
  number d1 = data.pool[eval(first(rest(v)), e)].d;

  return num(n0 + n1, d0 + d1);
}
ref n_gensym(ref v, env e)
{
  sym s = gensym();
  return symlist.pool[s].d;
}

inline static int islparen(int c)
{
  return c == '(' || c == '[';
}
inline static int isrparen(int c)
{
  return c == ')' || c == ']';
}
inline static int isparen(int c)
{
  return islparen(c) || isrparen(c);
}
inline static int isstring(int c)
{
  return c == '\"' || c == '\'';
}
inline static int isendsym(int c)
{
  return isparen(c) || isspace(c);
}

static char *read_varsize(FILE *f, int(*isendc)(int), boolean unget)
{
  char *s = calloc(BUFSIZE, 1);
  size_t i = 0;

  int c = getc(f);
repeat:
  do {
    if (ferror(f)) abort("Got error while grabbing chars");

    *(s + i++) = c;
  } while ((c = getc(f)) > 0 && i % BUFSIZE != 0 && !isendc(c));

  if (!isendc(c)) {
    s = realloc(s, i+BUFSIZE*sizeof(char));
    goto repeat;
  }
  else if (c != EOF) {
    s = realloc(s, (i+1)*sizeof(char));
    *(s + i) = 0;
  }
  if (unget) ungetc(c, f); /* This stops strings from breaking */

  tokens_add(s);
  return s;
}

static ref read_token(FILE *f)
/* Reads in a single token into input. Returns a ref (token) */
{
  int c = getc(f);
  if (isspace(c)) while ((c = getc(f)) > 0 && isspace(c));
  if (ferror(f)) abort("Caught error while attempting to find token!");

  if (islparen(c)) {
    return cons(NREF, NREF);
  }
  else if (isrparen(c)) {
    return NREF;
  }
  else if (isdigit(c))
  {
    number n = c - '0';
    number d = 0;
    while ((c = getc(f)) > 0 && isdigit(c)) {
      if (ferror(f)) abort("Caught error while reading in number!");
      if (n > UINT64_MAX) abort("Warning, input number too large for type");
      n *= 10;
      n += c - '0';
    }

    if (c == '.') {
      while ((c = getc(f)) > 0 && isdigit(c)) {
        if (ferror(f)) abort("Caught error while reading in number!");
        if (d > UINT64_MAX) abort("Warning, input number too large for type");
        d *= 10;
        d += c - '0';
      }
    } else if (!isendsym(c)) {
      abort("Caught non-digit character as part of number type");
    }
    ungetc(c, f);
    return num(n, d);
  }
  else if (isstring(c)) {
    return string(read_varsize(f, isstring, 0));
  }
  else {
    ungetc(c, f);
    return symbol(read_varsize(f, isendsym, 1));
  }
}

static ref read(FILE *f, ref d)
/* Reads in either an ordinary expr or a sexpr */
{
  ref root = d != NREF ? d : read_token(f);
  ref cp = root;
  ref tp;

  if (type(root) == CONS)
  {
    while (1) {
      tp = read_token(f);
      if (tp == NREF) break;
      else if (type(tp) == CONS)
        tp = read(f, tp);

      if (data.pool[cp].car == NREF) {
        data.pool[cp].car = tp;
      }
      else {
        data.pool[cp].cdr = cons(tp, NREF);
        cp = data.pool[cp].cdr;
      }
    }
  }
  return root;
}

static int repl(int debug)
{
  ref p, e;
  while (1) {
    printf("λ ");
    p = read(stdin, NREF);
    if (type(p) == SYMBOL && data.pool[p].sym == SREPLABORT) break;
    e = eval(p, GLOBALENV);
    pprint(GLOBALENV);

    print_types(e);
    pprint(e);

    tokens_clean();
  }

  return 0;
}
